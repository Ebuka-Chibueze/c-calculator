﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        private Color saveBG ,changeBG;
        public Form1()
        {
            InitializeComponent();
            tb1.BackColor = Color.White;
            tb2.BackColor = Color.White;
            tb3.BackColor = Color.White;
            bAdd.BackColor = Color.White;
            bSub.BackColor = Color.White;
            bDiv.BackColor = Color.White;
            bMul.BackColor = Color.White;
            radioButton1.BackColor = Color.White;
            radioButton2.BackColor = Color.White;
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            double v1, v2;
            bool b1, b2;
            b1 = Double.TryParse(tb1.Text, out v1); 
            b2 = Double.TryParse(tb2.Text, out v2);
            if (b1 && b2)
            {
                tb3.Text = (Convert.ToDouble(tb1.Text) + Convert.ToDouble(tb2.Text)).ToString();
            }
            else
            {
                tb3.Text = "Invalid Value";
            }
        }

        private void bSub_Click(object sender, EventArgs e)
        {

            double v1, v2;
            bool b1, b2;
            b1 = Double.TryParse(tb1.Text, out v1);
            b2 = Double.TryParse(tb2.Text, out v2);
            if (b1 && b2)
            {
                tb3.Text = (Convert.ToDouble(tb1.Text) - Convert.ToDouble(tb2.Text)).ToString();
            }
            else
            {
                tb3.Text = "Invalid Value";
            }
        }

        private void bMul_Click(object sender, EventArgs e)
        {

            double v1, v2;
            bool b1, b2;
            b1 = Double.TryParse(tb1.Text, out v1);// converts text input in tb1 into DOuble v1
            b2 = Double.TryParse(tb2.Text, out v2);
            if (b1 && b2) // if it is true then continue -->if both are doubles
            {
                tb3.Text = (Convert.ToDouble(tb1.Text) * Convert.ToDouble(tb2.Text)).ToString();
            }
            else
            {
                tb3.Text = "Invalid Value";
            }
        }

        private void bDiv_Click(object sender, EventArgs e)
        {

            double v1, v2;
            bool b1, b2;
            b1 = Double.TryParse(tb1.Text, out v1);
            b2 = Double.TryParse(tb2.Text, out v2);
            if ((b1 && b2) &&v2 !=0 )
            {
                tb3.Text = (Convert.ToDouble(tb1.Text) / Convert.ToDouble(tb2.Text)).ToString();
            }
                else if ((b1 && b2) &&v2 ==0 )
                {
                    tb3.Text = "You can't divide by Zero.";
                }
            else 
            {
                tb3.Text = "Invalid Value";
            }
        }


        private void tb1_TextChanged(object sender, EventArgs e)
        {
            double tmp;
            if (Double.TryParse(tb1.Text, out tmp))
                tb1.BackColor = saveBG;
            else
                tb1.BackColor = Color.Red;

            if (Double.TryParse(tb1.Text, out tmp) && Double.TryParse(tb2.Text, out tmp))
                bAdd.Enabled = true;
            else
                bAdd.Enabled = false;
        }

        private void tb2_TextChanged(object sender, EventArgs e)
        {
            double tmp;
            if (Double.TryParse(tb2.Text, out tmp))
                tb2.BackColor = saveBG;
            else
                tb2.BackColor = Color.Red;
            if (Double.TryParse(tb1.Text, out tmp) && Double.TryParse(tb2.Text, out tmp))
                bAdd.Enabled = true;
            else
                bAdd.Enabled = false;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            saveBG = tb1.BackColor;
        }

        

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            label4.Text = "X="+e.X.ToString();
            label5.Text = "Y=" + e.Y.ToString();
        }

       
        //Gaudy Button form
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Control ctrl = ((Control)sender);
            switch (ctrl.BackColor.Name)
            {
                
                default:
                    ctrl.BackColor = Color.Red;
                    tb1.BackColor = Color.Yellow;
                    tb2.BackColor = Color.Purple;
                    tb3.BackColor = Color.Yellow;
                    bAdd.BackColor = Color.Orange;
                    bSub.BackColor = Color.Blue;
                    bDiv.BackColor = Color.Green;
                    bMul.BackColor = Color.Red;
                    radioButton2.BackColor = Color.Red;

                    break;
            }
        }
        //Traditional Button form
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Control ctrl = ((Control)sender);
            switch (ctrl.BackColor.Name)
            {
                
                default:
                    ctrl.BackColor = Color.White;
                    tb1.BackColor = Color.White;
                    tb2.BackColor = Color.White;
                    tb3.BackColor = Color.White;
                    bAdd.BackColor = Color.White;
                    bSub.BackColor = Color.White;
                    bDiv.BackColor = Color.White;
                    bMul.BackColor = Color.White;
                    radioButton1.BackColor = Color.White;

                    break; 
            }
        }
    }
}
